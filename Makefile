OUT = out
PNG_DIR = $(OUT)/png
PDF_DIR = $(OUT)/pdf
PNG = $(patsubst %.svg,$(PNG_DIR)/%.png, $(wildcard *.svg))
PDF = $(patsubst %.svg,$(PDF_DIR)/%.pdf, $(wildcard *.svg))

.PHONY: all
all: $(PNG_DIR) $(PDF_DIR) $(PNG) $(PDF)

$(PNG_DIR): $(OUT)
	mkdir -p $(PNG_DIR)

$(PDF_DIR): $(OUT)
	mkdir -p $(PDF_DIR)

$(OUT):
	mkdir -p $(OUT)

# ***** .svg to .png *****
$(OUT)/png/%.png: %.svg
	@echo $@
	@inkscape --export-png="$@" --export-dpi=300 $<

# ***** .svg to .pdf *****
$(OUT)/pdf/%.pdf: %.svg
	@echo $@
	@inkscape --export-pdf="$@" $<
	exiftool "-ModifyDate<now" "-CreateDate<now" -overwrite_original $@

.PHONY: clean
clean:
	rm -r $(OUT)
